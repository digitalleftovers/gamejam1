package com.digitalleftovers.gj1;

import java.awt.Canvas;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.image.BufferStrategy;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferInt;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JFrame;

import com.digitalleftovers.gj1.Entities.Entity;
import com.digitalleftovers.gj1.Entities.Player;
import com.digitalleftovers.gj1.graphics.Screen;
import com.digitalleftovers.gj1.inputHandler.Keyboard;
import com.digitalleftovers.gj1.level.FirstLevel;
import com.digitalleftovers.gj1.level.Level;
import com.digitalleftovers.gj1.level.TitleLevel;
import com.digitalleftovers.gj1.menu.MainMenu;
import com.digitalleftovers.gj1.menu.Menu;

public class Game extends Canvas implements Runnable {
	private static final long serialVersionUID = 1L;

	public static int totalEntities = 0;
	public static final int WIDTH = 300;
	public static final int HEIGHT = WIDTH / 16 * 9;
	public static final int SCALE = 3;

	public static String title = "Volcanist";

	public static boolean running = false;

	private BufferedImage image = new BufferedImage(WIDTH, HEIGHT, BufferedImage.TYPE_INT_RGB);
	private int[] pixels = ((DataBufferInt) image.getRaster().getDataBuffer()).getData();

	private JFrame frame;
	private Thread thread;
	public static Screen screen;
	private Keyboard key;
	private Player player;
	public static Level level;
	public static Menu menu;

	public static boolean isPlaying = false; 

	public static int x;
	public static int y;

	public Game() {
		Dimension display = new Dimension(WIDTH * SCALE, HEIGHT * SCALE);

		setMaximumSize(display);
		setMinimumSize(display);
		setPreferredSize(display);

		frame = new JFrame();
		screen = new Screen(WIDTH, HEIGHT);
		key = new Keyboard();
		menu = new MainMenu(key, 0);
		player = new Player((WIDTH / 2) - 16, (HEIGHT / 2) - 16, key); 
		level = new FirstLevel("/levels/firstLevel.png");

		// level = new RandomLevel(64, 64);

		addKeyListener(key);

	}

	public synchronized void start() {
		running = true;
		thread = new Thread(this, "display");
		thread.start();

	}

	public synchronized void stop() {
		running = false;
		try {
			thread.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	public void run() {

		long lastTime = System.nanoTime();
		long timer = System.currentTimeMillis();
		final double ns = 1000000000.0 / 60.0;
		double delta = 0;
		int frames = 0;
		int updates = 0;
		while (running) {
			long now = System.nanoTime();
			delta += (now - lastTime) / ns;
			lastTime = now;
			while (delta >= 1) {
				update();
				updates++;
				delta--;
			}
			render();
			frames++;

			if (System.currentTimeMillis() - timer > 1000) {
				timer += 1000;
				System.out.println("ups: " + updates + ", fps: " + frames + ".");
				updates = 0;
				frames = 0;
			}
		}
	}

	public void update() {
		key.update();
		if (!isPlaying) menu.update();
		if (isPlaying) player.update();


	}

	public void render() {
		BufferStrategy bs = getBufferStrategy();
		if (bs == null) {
			createBufferStrategy(3);
			return;
		}
		Graphics g = bs.getDrawGraphics();
		screen.graphics(g);

		screen.clear();
		level.render(x, y, screen);
		if (isPlaying) player.render(screen);

		for (int i = 0; i < pixels.length; i++) {
			pixels[i] = screen.pixels[i];
		}

		g.drawImage(image, 0, 0, getWidth(), getHeight(), null);
		if (!isPlaying)menu.render(screen);
		g.dispose();
		bs.show();

	}

	public static List<String> CreateEntityList() {
		List<String> EntityList = new ArrayList<String>();
		return EntityList;
	}

	public static List<String> LoopThroughEntityList(List<String> EntityList) {
		if (EntityList != null) {
			for (int i = 0; i < EntityList.size(); i++) {
				System.out.println(EntityList);
			}
		}
		return EntityList;
	}

	public static List<String> AddToEntityList(List<String> EntityList, String ObjName) {
		EntityList.add(Entity.ObjName);
		return EntityList;
	}

	public static void main(String[] args) {
		Game game = new Game();

		Game.LoopThroughEntityList(CreateEntityList());

		game.frame.setResizable(false);
		game.frame.setTitle(title);
		game.frame.add(game);
		game.frame.pack();
		game.frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		game.frame.setLocationRelativeTo(null);
		game.frame.setVisible(true);
		game.start();
	}

}
