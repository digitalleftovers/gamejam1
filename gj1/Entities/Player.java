package com.digitalleftovers.gj1.Entities;

import com.digitalleftovers.gj1.Game;
import com.digitalleftovers.gj1.graphics.Screen;
import com.digitalleftovers.gj1.graphics.Sprite;
import com.digitalleftovers.gj1.inputHandler.Keyboard;

public class Player {

	private static int x, y;
	private static float dy;

	public int xa, ya;

	public boolean mayJumpAgain = false;
	public boolean isOnSolidGround = false;

	private Keyboard input;
	public static int dir;

	private int anim = 0;

	public Sprite sprite;

	public Player(int x, int y, Keyboard input) {
		this.x = x;
		this.y = y;
		this.input = input;
	}

	public void update() {
		xa = 0;

		if (anim < 10000) {
			anim++;
		} else {
			anim = 0;
		}

		if (Keyboard.right == true) xa++;
		if (Keyboard.left == true) xa--;

		if (xa != 0) {
			move(xa);
		} else {
			dir = 1;
		}

		if (input.left) {
			Game.x -= 2;
		}

		if (input.right) {
			Game.x += 2;
		}
		if (isOnSolidGround) {
			if (input.jump) {
				if (mayJumpAgain) {
					dy = -7;
					mayJumpAgain = false;
					isOnSolidGround = false;
				}
			} else {
				mayJumpAgain = true;
			}

			checkCollision();
		}

		if (!isOnSolidGround) dy += 0.5;
		if (input.jump && isOnSolidGround && dy > 0) dy += 0.1;
		if (dy > 5) dy = 5;

		Game.y += Math.round(dy);

		if (Game.y > (Screen.height / 2) - 32) { // This should be true when collided with tile
			y = (Screen.height / 2) - 32;

			isOnSolidGround = true;
		} else {
			isOnSolidGround = false;
		}

		if (isOnSolidGround) {
			dy = 0;
		}

	}

	private void checkCollision() {

	}

	public void render(Screen screen) {
		if (dir == 0) {
			if (anim % 20 < 10) {
				sprite = Sprite.player_left3;
			} else {
				sprite = Sprite.player_left1;
			}
		}
		if (dir == 2) {
			if (anim % 20 < 10) {
				sprite = Sprite.player_right3;
			} else {
				sprite = Sprite.player_right1;
			}
		}
		if (dir == 1) sprite = Sprite.player_stat;
		screen.renderPlayer(x, y, sprite);
	}

	public void move(int xa) {
		if (xa < 0) dir = 0;
		if (xa > 0) dir = 2;
	}
}
