package com.digitalleftovers.gj1.Entities;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import com.digitalleftovers.gj1.Game;
import com.digitalleftovers.gj1.graphics.Screen;
import com.digitalleftovers.gj1.graphics.Sprite;

public class Entity {

	public static boolean HitSomething = false;
	public static boolean Collidable;
	public static boolean Destructable;
	public static String ObjName;
	public static Sprite Sprite;
	public static int EntityID;
	public static int EntityType;
	public static int Opacity;
	public static int DropChance;
	public static int ProjectileSpeed;
	public static int x0, x1, y0, y1;
	public static int velDir;
	public static int x;
	public static int y;
	public String ObjName1;
	public static String CleanEntityList;
	
	public Sprite collidableSprites[];

	public Entity(String ObjName, Sprite Sprite, Sprite Left, Sprite Right, boolean Collidable, int x0, int x1, int y0, int y1/*, int x, int y, boolean Collidable, , int Opacity, boolean Destructable, int entityType, int ProjectileSpeed, int dir, int DropChance*/) {
		
		Game.totalEntities++;
		EntityID = Game.totalEntities;
		List<String> EntityList = new ArrayList<String>();
		EntityList.add(ObjName);
		LoopThroughEntityList(EntityList);
	}
	
	public static void add(String ObjName){

		Game.AddToEntityList(Game.CreateEntityList(), ObjName);
	}

	public static boolean isCollidable() {
		if (Collidable == true) {
			return true;
		} else {
			return false;
		}
	}

	public static boolean isDestructable() {
		if (Destructable == true) {
			return true;
		} else {
			return false;
		}
	}

	public static int getEntityID() {
		return EntityID;
	}

	public static int getEntityType() {
		return EntityType;
	}

	public static int getOpacity() {
		return Opacity;
	}

	public static int getDropChance() {
		return DropChance;
	}

	public void changeSprite(Sprite NewSprite) {
		Sprite = NewSprite;
	}

	public boolean lootDrop() {
		// One idea
		Random rn = new Random();
		int RandomNumber = rn.nextInt(DropChance - 0);

		if (RandomNumber == 5) {

		}
		return false;
	}
	
	public void render(Screen screen) {
		Screen.renderEntity(x, y, this);
	}
	
	public boolean collisionDetect() {
		return false;
	}
	
	public static List<String> LoopThroughEntityList(List<String> EntityList) {
		if (EntityList != null) {
			for (int i = 0; i < EntityList.size(); i++) {
				CleanEntityList = EntityList.toString().replace("[", "").replace("]", "");
				CleanEntityList += ".";
				
				
			}
		}
		return EntityList;
	}
}
