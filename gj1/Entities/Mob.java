package com.digitalleftovers.gj1.Entities;
import com.digitalleftovers.gj1.graphics.Screen;
import com.digitalleftovers.gj1.graphics.Sprite;

public class Mob extends Entity {
	
	public static Sprite LeftProjectileSprite;
	public static Sprite RightProjectileSprite;
	
	public Mob(Sprite Sprite, Sprite LeftProjectileSprite, Sprite RightProjectileSprite, int x, int y, boolean Collidable, int Opacity,
			boolean Destructable, int entityType, int ProjectileSpeed, int dir, int DropChance) {
		
		
		super(ObjName, Sprite/*, x, y, Collidable, Opacity, x0, x1, y0, y1, Destructable, entityType, ProjectileSpeed, dir, DropChance*/, RightProjectileSprite, RightProjectileSprite, true);
		
	}

	
	public void Projectile() {
		if (Player.dir == 0) {
			Screen.renderBullet(x, y, LeftProjectileSprite);
		}

		if (Player.dir == 2) {
			Screen.renderBullet(x, y, RightProjectileSprite);
		}

		while (!HitSomething) {

			if(Player.dir == 0) {
				Screen.renderBullet(--x, y, LeftProjectileSprite);
			}
			
			if(Player.dir == 2) {
				Screen.renderBullet(--x, y, RightProjectileSprite);
			}
		//Sprite LeftProjectileSprite, Sprite RightProjectileSprite,
		}
	}

}
