package com.digitalleftovers.gj1.menu;

import java.awt.Color;

import com.digitalleftovers.gj1.Game;
import com.digitalleftovers.gj1.graphics.Screen;
import com.digitalleftovers.gj1.inputHandler.Keyboard;

public class InfoMenu extends Menu {
	
	private Keyboard input;

	public InfoMenu(Keyboard input) {
		super(input);
	}

	public void update() {
		if (input.esc) Game.menu = new MainMenu(input,2);

	}

	public void render(Screen screen) {
		
		screen.renderText("This project was originally going to take 48", 15, 80, 40, Color.white);
		screen.renderText("hours. However we didn't understand", 15, 120, 40, Color.white);
		screen.renderText("entities and got stuck.", 15, 160, 40, Color.white);
		
		screen.renderText("Programmers: ", 15, 240, 40, Color.white);
		screen.renderText("Alec Ayre", 310, 240, 40, Color.white);
		screen.renderText("Harry Rudolph", 310, 280, 40, Color.white);
		
		screen.renderText("Graphics:", 15, 360, 40, Color.white);
		screen.renderText("Daniel Jones", 310, 360, 40, Color.white);
		screen.renderText("Jordan Dixon", 310, 400, 40, Color.white);

		// This project was originally going to take 48 hours. However we didn't understand entities and got stuck.
		// Programmers: Alec Ayre
		// Harry Rudolph
		// Graphics: Daniel Jones
		// Jordan Dixon
	}

}
