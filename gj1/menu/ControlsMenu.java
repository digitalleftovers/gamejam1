package com.digitalleftovers.gj1.menu;

import com.digitalleftovers.gj1.Game;
import com.digitalleftovers.gj1.graphics.Screen;
import com.digitalleftovers.gj1.inputHandler.Keyboard;

public class ControlsMenu extends Menu {

	private Keyboard input;

	public ControlsMenu(Keyboard input) {
		super(input);
	}

	public void update() {
		if (input.esc) Game.menu = new MainMenu(input, 1);

	}

	public void render(Screen screen) {
		// render image here.
	}

}
