package com.digitalleftovers.gj1.menu;

import java.awt.Color;

import com.digitalleftovers.gj1.Game;
import com.digitalleftovers.gj1.graphics.Screen;
import com.digitalleftovers.gj1.inputHandler.Keyboard;
import com.digitalleftovers.gj1.level.SecondLevel;

public class MainMenu extends Menu {

	private Keyboard input;
	String[] options = { "Play", "Controls", "Info" };
	int selected;
	int timer = 0;

	public MainMenu(Keyboard input, int selected) {
		super(input);
		this.input = input;
		this.selected = selected;
	}

	public void update() {

		if (timer > 0) timer--;

		if (input.up && timer == 0) {
			selected--;
			timer = 10;
		}
		if (input.down && timer == 0) {
			selected++;
			timer = 10;
		}

		if (selected < 0) selected = 0;
		if (selected > 2) selected = 2;

		if (selected == 0) {
			options[selected] = "<Play>";
		} else {
			options[0] = "  Play";
		}

		if (selected == 1) {
			options[selected] = "<Controls>";
		} else {
			options[1] = "  Controls";
		}

		if (selected == 2) {
			options[selected] = "<Info>";
		} else {
			options[2] = "  Info";
		}

		if (selected == 0 && input.action) {
			Game.isPlaying = true;
			Game.level = new SecondLevel("/levels/secondLevel.png");
		}
		if (selected == 1 && input.action) Game.menu = new ControlsMenu(input);
		if (selected == 2 && input.action) Game.menu = new InfoMenu(input);
	}

	public void render(Screen screen) {
		for (int i = 0; i < options.length; i++) {
			screen.renderText(options[i], 400, 300 + i * 40, 40, Color.WHITE);
		}
	}

}
