package com.digitalleftovers.gj1.graphics;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;

import com.digitalleftovers.gj1.Entities.Entity;

public class Screen {

	public static int width;
	public static int height;
	public static int pixels[];
	public int tiles[];

	private int xOffset;
	private int yOffset;

	private Graphics g;

	public void graphics(Graphics g) {
		this.g = g;
	}

	public Screen(int width, int height) {
		this.width = width;
		this.height = height;
		pixels = new int[width * height];
	}

	public void renderTile(int xp, int yp, Sprite sprite) {
		xp -= xOffset;
		yp -= yOffset;
		for (int y = 0; y < sprite.SIZE; y++) {
			int ya = y + yp;
			for (int x = 0; x < sprite.SIZE; x++) {
				int xa = x + xp;
				if (xa < -sprite.SIZE || xa >= width || ya < 0 || ya >= height) break;
				if (xa < 0) xa = 0;
				pixels[xa + ya * width] = sprite.pixels[x + y * sprite.SIZE];
			}
		}
	}

	public void renderPlayer(int x2, int y2, Sprite sprite) {
		for (int y = 0; y < 32; y++) {
			if (y >= height || y < 0) break;
			int ya = y + y2;
			for (int x = 0; x < 32; x++) {
				if (x >= width || x < 0) break;
				int xa = x + x2;
				int col = sprite.pixels[x + y * 32];
				if (col != 0xffD67FFF) pixels[xa + ya * width] = col;
				if (xa + ya * width > pixels.length || xa + ya * width < 0) break;
			}
		}

	}

	// call this method in some loop that changes the xp and yp to change their position on the screen.

	public static void renderEntity(int xp, int yp, Sprite sprite) {
		for (int y = 0; y < Entity.Sprite.SIZE; y++) {
			if (y >= height || y < 0) break;
			int ya = y + yp;
			for (int x = 0; x < Entity.Sprite.SIZE; x++) {
				int xa = x + xp;
				int col = Entity.Sprite.pixels[x + y * 32];
				if (col != 0xffD67FFF) pixels[xa + ya * width] = col;
			}
		}
	}

	public static void renderBullet(int xp, int yp, Sprite sprite) {
		for (int y = 0; y < sprite.SIZE; y++) {
			if (y >= height || y < 0) break;
			int ya = y + yp;
			for (int x = 0; x < sprite.SIZE; x++) {
				int xa = x + xp;
				if (xa > width || xa < 0) break;
				int col = sprite.pixels[x + y * 16];
				if (col != 0xffD67FFF) pixels[xa + ya * width] = col;
			}
		}

	}

	public void renderText(String text, int x, int y, int size, Color color) {
		this.g.setColor(color);
		Font f = new Font("Verdana", 0, size);
		this.g.setFont(f);
		this.g.drawString(text, x, y);

	}

	public void clear() {
		for (int i = 0; i < pixels.length; i++) {
			pixels[i] = 0;
		}
	}

	public void setOffset(int xOffset, int yOffset) {
		this.xOffset = xOffset;
		this.yOffset = yOffset;
	}

}
