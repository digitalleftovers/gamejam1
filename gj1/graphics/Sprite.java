package com.digitalleftovers.gj1.graphics;


public class Sprite {
	public final int SIZE;
	private int x, y;
	public int pixels[];
	private SpriteSheet sheet;
	public int collidable; 

	public static Sprite lava1 = new Sprite(16, 0, 0, 1,SpriteSheet.textures);
	public static Sprite lava2 = new Sprite(16, 1, 0, 1,SpriteSheet.textures);

	public static Sprite lavaFlow1 = new Sprite(16, 0, 1,1, SpriteSheet.textures);

	public static Sprite rock1 = new Sprite(16, 0, 2,0, SpriteSheet.textures);
	public static Sprite rock2 = new Sprite(16, 0, 5, 0,SpriteSheet.textures);

	public static Sprite ore_iron = new Sprite(16, 0, 3,0, SpriteSheet.textures);
	public static Sprite ore_silver = new Sprite(16, 0, 4,0, SpriteSheet.textures);

	public static Sprite gem_rainbow = new Sprite(16, 0, 6,0, SpriteSheet.textures);
	public static Sprite gem_purple = new Sprite(16, 0, 7, 0,SpriteSheet.textures);

	public static Sprite cloud_snail = new Sprite(16, 0, 8, 1,SpriteSheet.textures);
	public static Sprite cloud_cat = new Sprite(16, 0, 9, 1,SpriteSheet.textures);
	public static Sprite cloud_animal = new Sprite(16, 0, 10,1, SpriteSheet.textures);
	public static Sprite cloud_dong = new Sprite(16, 1, 8, 1,SpriteSheet.textures);
	public static Sprite cloud_small = new Sprite(16, 1, 9, 1,SpriteSheet.textures);
	
	public static Sprite grass = new Sprite(16, 1, 10, 0,SpriteSheet.textures);
	public static Sprite grass_flower = new Sprite(16, 0, 11,0, SpriteSheet.textures);
	public static Sprite grass_mud = new Sprite(16, 1, 11, 0,SpriteSheet.textures);
	
	public static Sprite stone= new Sprite(16, 1, 9, 0,SpriteSheet.textures);
	public static Sprite stone_pillar = new Sprite(16, 1, 9, 0,SpriteSheet.textures);
	
	public static Sprite voidTile = new Sprite(16, 0x6D1EFF);

	public static Sprite player_stat = new Sprite(32, 0, 0, 2,SpriteSheet.characters);

	public static Sprite player_right1 = new Sprite(32, 0, 5, 2,SpriteSheet.characters);
	public static Sprite player_right2 = new Sprite(32, 0, 1,2, SpriteSheet.characters);
	public static Sprite player_right3 = new Sprite(32, 0, 6, 2,SpriteSheet.characters);

	public static Sprite player_left1 = new Sprite(32, 0, 3, 2,SpriteSheet.characters);
	public static Sprite player_left2 = new Sprite(32, 0, 2, 2,SpriteSheet.characters);
	public static Sprite player_left3 = new Sprite(32, 0, 4, 2,SpriteSheet.characters);

	//public static Sprite bulletLeft = new Sprite(16, 15, 0, SpriteSheet.textures);// decide where
	//public static Sprite bulletRight = new Sprite(16, 15, 0, SpriteSheet.textures);// decide where
	
	//Collidable 0 means player stands on it.
	//Collidable 1 
	//Collidable 2 player

	public Sprite(int size, int y, int x, int collidable, SpriteSheet sheet) {
		SIZE = size;
		pixels = new int[SIZE * SIZE];
		this.x = x * size;
		this.y = y * size;
		this.collidable = collidable;
		this.sheet = sheet;
		load();
	}

	public Sprite(int size, int colour) {
		SIZE = size;
		pixels = new int[SIZE * SIZE];
		setColour(colour);
	}

	private void setColour(int colour) {
		for (int i = 0; i < SIZE * SIZE; i++) {
			pixels[i] = colour;
		}
	}

	private void load() {
		for (int y = 0; y < SIZE; y++) {
			for (int x = 0; x < SIZE; x++) {
				pixels[x + y * SIZE] = sheet.pixels[(x + this.x) + (y + this.y) * sheet.SIZE];
			}
		}
	}

	//Because each sprite is static you can have render methods for each of them.
	public void render(int x, int y, Screen screen) {
		screen.renderTile(x << 4, y << 4, this);
	}
	
}
