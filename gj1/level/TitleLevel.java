package com.digitalleftovers.gj1.level;

import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.imageio.ImageIO;

import com.digitalleftovers.gj1.graphics.Sprite;

public class TitleLevel extends Level{
	private int levelPixels[];

	public TitleLevel(String path) {
		super(path);
	}

	protected void loadLevel(String path) {
		try {
			BufferedImage image = ImageIO.read(FirstLevel.class.getResource(path));
			int w = image.getWidth();
			int h = image.getHeight();
			sprites = new Sprite[w * h];
			levelPixels = new int[w * h];
			image.getRGB(0, 0, w, h, levelPixels, 0, w);

		} catch (IOException e) {
			e.printStackTrace();
			System.out.println("Could not load level.");
		}

	}

	
	// rock1 = 0xFFFFFFFF

	protected void generate() {
		for (int i = 0; i < levelPixels.length; i++) {
			if (levelPixels[i] == 0xFFFFFFFF) sprites[i] = Sprite.rock1;



		}
	}
	
}
