package com.digitalleftovers.gj1.level;

import com.digitalleftovers.gj1.graphics.Screen;
import com.digitalleftovers.gj1.graphics.Sprite;

public class Level {

	protected int width;
	protected int height;
	public static int tiles[] = new int[64 * 64];
	public Sprite[] sprites;

	public static int collidableTiles[] = new int[64 * 64];

	public Level(int width, int height) {
		this.width = width;
		this.height = height;
		tiles = new int[width * height];
		generate();
	}

	public Level(String path) {
		loadLevel(path);
		createCollidableArray();
		generate();
	}

	protected void loadLevel(String path) {
	}

	protected void generate() {
	}

	public void render(int xScroll, int yScroll, Screen screen) {
		screen.setOffset(xScroll, yScroll); // Scroll is location of player.
		int x0 = xScroll >> 4;
		int x1 = (xScroll + screen.width + 16) >> 4;
		int y0 = yScroll >> 4;
		int y1 = (yScroll + screen.height + 16) >> 4;

		for (int y = y0; y < y1; y++) {// Rendering all pixels between top and bottom, left and right
			for (int x = x0; x < x1; x++) {// x and y are the tiles you are currently looping.
				if (x + y * 32 < 0 || x + y * 32 >= 32 * 32) { // 32 is size of map atm
					Sprite.voidTile.render(x, y, screen);
					continue;
				}
				sprites[x + y * 32].render(x, y, screen);
			}
		}
	}
	
	protected void createCollidableArray(){
	}

	public Sprite getSprite(int x, int y) { // Converts int array to tiles
		if (x < 0 || y < 0 || x >= width || y >= height) return Sprite.voidTile;
		if (tiles[x + y * width] == 0) return Sprite.lava1;
		if (tiles[x + y * width] == 1) return Sprite.lava2;
		if (tiles[x + y * width] == 2) return Sprite.rock1;
		if (tiles[x + y * width] == 3) return Sprite.rock2;
		if (tiles[x + y * width] == 4) return Sprite.ore_iron;
		if (tiles[x + y * width] == 5) return Sprite.ore_silver;
		if (tiles[x + y * width] == 6) return Sprite.gem_rainbow;
		if (tiles[x + y * width] == 7) return Sprite.gem_purple;
		return Sprite.voidTile;

	}

}
