package com.digitalleftovers.gj1.level;

import java.util.Random;

public class RandomLevel extends Level {

	public RandomLevel(int width, int height) {
		super(width, height);
	}

	public static Random rand = new Random();

	protected void generate() {
		for (int y = 0; y < height; y++) {
			for (int x = 0; x < width; x++) {
				tiles[x+y*width] = rand.nextInt(8);
			}

		}

	}
}
