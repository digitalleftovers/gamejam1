package com.digitalleftovers.gj1.level;

import java.awt.image.BufferedImage;
import java.io.IOException;
import javax.imageio.ImageIO;
import com.digitalleftovers.gj1.graphics.Sprite;

public class FirstLevel extends Level {
	private int levelPixels[];

	public FirstLevel(String path) {
		super(path);
	}

	protected void loadLevel(String path) {
		try {
			BufferedImage image = ImageIO.read(FirstLevel.class.getResource(path));
			int w = image.getWidth();
			int h = image.getHeight();
			sprites = new Sprite[w * h];
			levelPixels = new int[w * h];
			image.getRGB(0, 0, w, h, levelPixels, 0, w);

		} catch (IOException e) {
			e.printStackTrace();
			System.out.println("Could not load level.");
		}

	}

	/*
	 * lava1 = 0xFFFF0000 
	 * lava2 = 0xFFFFBB00
	 * 
	 * lavaFlow1 = 0xFF997000
	 * 
	 * rock1 = 0xFFFF07F2 
	 * rock2 = 0xFFB505AC
	 * 
	 * ore_iron = 0xFF808080 
	 * ore_silver = 0xFFBCBCBC
	 * 
	 * gem_rainbow = 0xFFFFFF00 
	 * gem_purple = 0xFF954FFF
	 * 
	 * cloud_snail cloud_cat cloud_animal cloud_dong cloud_small
	 */
	protected void generate() {
		for (int i = 0; i < levelPixels.length; i++) {
			if (levelPixels[i] == 0xFFFF0000) sprites[i] = Sprite.rock1;
			if (levelPixels[i] == 0xFFFFBB00) sprites[i] = Sprite.lava2;
			if (levelPixels[i] == 0xFF997000) sprites[i] = Sprite.lavaFlow1;
			if (levelPixels[i] == 0xFFFF07F2) sprites[i] = Sprite.lava1;
			if (levelPixels[i] == 0xFFB505AC) sprites[i] = Sprite.rock2;			
			if (levelPixels[i] == 0xFF808080) sprites[i] = Sprite.ore_iron;
			if (levelPixels[i] == 0xFFBCBCBC) sprites[i] = Sprite.ore_silver;
			if (levelPixels[i] == 0xFFFFFF00) sprites[i] = Sprite.gem_rainbow;
			if (levelPixels[i] == 0xFF954FFF) sprites[i] = Sprite.gem_purple;
		}
	}
	

}
